#! /usr/bin/env python3

import re
import preprocessing


def read_dic(dictonary_file, is_it_harmony_dic=False):
    dic = {}
    with open(dictonary_file, "r") as file:
        for line in file.readlines():
            line.strip()
            s, f = line.split(":")
            if is_it_harmony_dic:
                dic[s] = int(f) % 12
            else:
                dic[s] = int(f)
    return dic


def read_voices_distribution(dictonary_file):
    dic = {}
    with open(dictonary_file, "r") as file:
        for line in file.readlines():
            line.strip()
            s, f = line.split(" : ")
            dic[s] = int(f)
    return dic


def read(source, dictonary, h_dictonary, voice=None):
    h_distribution = []
    m_distribution = []
    header = []
    # dic = read_dic(dictonary)
    # with open(source, 'r') as file:
    #     for line in file.readlines():
    #         if str(line).startswith("!") or str(line).startswith("="):
    #             # comment
    #             # end of bar
    #             continue
    #         elif str(line).startswith("*"):
    #             # tonality and metre
    #             [header.append(item) for item in line.split("\t")]
    #         else:
    #             distribution = line.split("\t")
    #             m = -1
    #             try:
    #                 m = float(toTone(voice if voice is not None else distribution[len(distribution)], dictonary))
    #             except:
    #                 if len(harmony_distribution) > 1:
    #                     m = harmony_distribution[len(harmony_distribution) - 1].melody
    #                 else:
    #                     m = 0
    #             h = HarmonyDistribution(str(distribution[0]), m)
    #             del distribution[0:1]
    #             del distribution[len(distribution) - 1:len(distribution)]
    #             for s in distribution:
    #                 try:
    #                     h.tones.append(float(toTone(s, dictonary)))
    #                 except:
    #                     continue
    #             harmony_distribution.append(h)
    with open(source, 'r') as file:
        for line in file.readlines():
            if str(line).startswith("!") or str(line).startswith("=") or str(line).startswith("*"):
                # comment
                # end of bar
                # tonality and metre
                continue
            else:
                harmony_distribution = (" ".join(line.split())).split(" ")

                h_distribution.append(toTone(harmony_distribution[0], h_dictonary))
                del harmony_distribution[0:1]

                m = float(0)
                try:
                    m = float(toTone(harmony_distribution[voice if voice is not None else harmony_distribution[-1]], dictonary))
                except:
                    m = float(0)
                if m == float(0) and len(m_distribution) >= 1:
                    m = preprocessing.remove_1(m, m_distribution[-1])
                m_distribution.append(m)

    # tonality, tonality_level = tonality_detector(header)

    return m_distribution, h_distribution


def remove_pauses(vec_1, vec_2):
    for i in range(0, len(vec_1)):
        if vec_1[i] != -1:
            continue
        elif i != 0 and vec_1[i - 1] != -1:
            vec_1[i] = vec_1[i - 1]
        elif vec_1[i] == -1:
            vec_1[i] = 0

        if vec_2[i] != -1:
            continue
        elif i != 0 and vec_2[i - 1] != -1:
            vec_2[i] = vec_2[i - 1]
        elif vec_2[i] == -1:
            vec_2[i] = 0
    return vec_1, vec_2


# def toTone(s, dic):
#     # todo duration => weight
#     # print(dic)
#     # print(s + " :: " + s.replace("\\", "/"))
#     for tone, f in dic.items():
#         # print("-- " + s + " :: " + tone)
#         regexp = re.compile(r'' + tone + '')
#         if s == '.':
#             return float(0)
#         elif regexp.search(s):
#             print("Find: " + str(f))
#             return f
#         else:
#             return float(-1)
def toTone(s, dic):
    s = s.replace("\\", '')
    out = float(-1)
    for tone, f in dic.items():
        if re.search(r'' + tone + '', s):
            # print("Find: " + str(s) + ": " + str(tone) + ": " + str(f))
            return f
        else:
            continue
    return out


def toToneHarmony(s, dic):
    # todo duration => weight
    for tone, f in dic.items():
        if s == tone:
            return f


class HarmonyDistribution:

    def __init__(self, harmony, melody):
        self.harmony = harmony
        self.melody = melody
        self.tones = []

    def print(self):
        print(self.harmony + " " + str(self.melody) + " " + str(self.tones))


def tonality_detector(header):
    signs = []
    for i, item in enumerate(header):
        if str(item).startswith("*k["):
            signs.append(str(item).count("-")) # flats count
            signs.append(str(item).count("#")) # crosses count
            break
    if len(signs) > 1:
        if signs[0] > 0:
            # flats
            if signs[0] == 1:
                return "F-dur lub d-moll", 2.5
            elif signs[0] == 2:
                return "B-dur lub g-moll", 5
            elif signs[0] == 3:
                return "Es-dur lub c-moll", 1.5
            elif signs[0] == 4:
                return "As-dur lub f-moll", 4
            elif signs[0] == 5:
                return "Des-dur lub b-moll", .5
            elif signs[0] == 6:
                return "Ges-dur lub es-moll", 3
            elif signs[0] == 7:
                return "Ces-dur lub as-moll", 5.5
            else:
                return "Flats: {0}".format(signs[0])
        if signs[1] > 0:
            # corsses
            if signs[1] == 1:
                return "G-dur lub e-moll", 3.5
            elif signs[1] == 2:
                return "D-dur lub h-moll", 1
            elif signs[1] == 3:
                return "A-dur lub fis-moll", 4.5
            elif signs[1] == 4:
                return "E-dur lub cis-moll", 2
            elif signs[1] == 5:
                return "H-dur lub gis-moll", 5.5
            elif signs[1] == 6:
                return "Fis-dur lub dis-moll", 3
            elif signs[1] == 7:
                return "Cis-dur lub ais-moll", .5
        else:
            return "C-dur lub a-moll", 0
    else:
        return "Unknown tonality..."


def data_compiler(data, dictonary):
    result_m = []
    result_h = []
    for item in data:
        result_m.append(item.melody)
        result_h.append(toToneHarmony(str(item.harmony), dictonary))
    return result_m, result_h
