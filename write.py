#! /usr/bin/env python3


def write(source, output_path, h_dictonary, predictions, voice=None):
    n = 0
    with open(output_path, 'w') as output:
        with open(source, 'r') as file:
            for line in file.readlines():
                if str(line).startswith("!") or str(line).startswith("=") or str(line).startswith("*"):
                    output.write(line)
                else:
                    harmony_distribution = (" ".join(line.split())).split(" ")
                    output.write(str(FloatToTone(predictions[n], h_dictonary)) + " "
                                 + harmony_distribution[voice if voice is not None else harmony_distribution[-1]] + "\n")
                    n += 1


def FloatToTone(f, dic):
    for tone, i in dic.items():
        if i == f:
            return tone
        else:
            continue
    return '.'
