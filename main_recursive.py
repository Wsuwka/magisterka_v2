#! /usr/bin/env python3

import read, write, glob, os, preprocessing, math
import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser

from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, Dropout, Embedding
from keras.utils import np_utils

# Preprocessing of data. Choice of voice treated as melody:
# $ python3 main_recursive.py -m piano_sonata_6
# Model training:
# $ python3 main_recursive.py -t piano_sonata_6
# Model output comparison with thema of variations:
# $ python3 main_recursive.py -c piano_sonata_6
#
# optional params: -p path_to_model.h5 [path for trained model, default value: lstm_harmonization_model.h5]

batch_size = 12
features = 3
dropout_propability = .5 # prawdopowobieństwo przycinania węzłów


def main():
    parser = OptionParser()
    parser.add_option('-m', action="store", type="string", dest="vdistribution", help="Module for preparing dataset of melodies.")
    parser.add_option('-t', action="store", type="string", dest="training", help="Module for training the model.")
    parser.add_option('-c', action="store", type="string", dest="prediction", help="Module for making prediction with trained model.")
    parser.add_option('-p', action="store", type="string", dest="path", help="Path to input dataset.")
    (options, args) = parser.parse_args()

    model_path = 'lstm_harmonization_model.h5'
    if options.path:
        model_path = options.path

    if options.vdistribution:
        dic = {}
        for file in glob.glob(os.getcwd() + "/kern_files/" + options.vdistribution + "/v*.krn"):
            # choice("kern_files/piano_sonata_6/v1.krn", "kern_files/dictonary.txt")
            print(file)
            dic[file] = int(preprocessing.choice(file, "kern_files/dictonary.txt"))
        file = open('voices_distribution.txt', 'w')
        for v, k in dic.items():
            file.write(str(v) + " : " + str(k) + "\n")
        file.close()
    elif options.training:
        train_m = []
        train_h = []
        valid_m = []
        valid_h = []
        dictonary = read.read_dic("kern_files/dictonary.txt")
        H_dictonary = read.read_dic("kern_files/dictonary.txt", True)
        voices_distribution = read.read_voices_distribution("voices_distribution.txt")
        print('Loading data...')
        i = 0
        for file in glob.glob(os.getcwd() + "/kern_files/" + options.training + "/v*.krn"):
            (melody, harmony) = read.read(file, dictonary, H_dictonary, voices_distribution[file])
            (melody, harmony) = read.remove_pauses(melody, harmony)
            if i < 9:
                [train_m.append(i) for i in melody]
                [train_h.append(i) for i in harmony]
            else:
                [valid_m.append(i) for i in melody]
                [valid_h.append(i) for i in harmony]
            i += 1

        for i in range(0, len(train_m) % (features * batch_size)):
            del(train_m[-1])
            del(train_h[-1])
        for i in range(0, len(valid_m) % (features * batch_size)):
            del(valid_m[-1])
            del(valid_h[-1])

        train_m = np.reshape(train_m, (int(len(train_m) / (features * batch_size)), batch_size, features))
        train_h = np.reshape(train_h, (int(len(train_h) / (features * batch_size)), batch_size, features))

        valid_m = np.reshape(valid_m, (int(len(valid_m) / (features * batch_size)), batch_size, features))
        valid_h = np.reshape(valid_h, (int(len(valid_h) / (features * batch_size)), batch_size, features))
        # ------------------------------------------------ #

        model = Sequential()
        # model.add(Embedding(1024, output_dim=(1, 1)))
        model.add(LSTM(256, input_shape=(batch_size, features), return_sequences=True))
        model.add(Dropout(dropout_propability))
        model.add(Dense(features, activation='softmax'))

        model.compile(loss='binary_crossentropy',
                      optimizer='rmsprop')
        history = model.fit(train_m, train_h, batch_size=batch_size, epochs=200, validation_data=(valid_m, valid_h))

        model.save(model_path)
        print('Trained model saved at path: ' + model_path)

        loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' not in s]
        val_loss_list = [s for s in history.history.keys() if 'loss' in s and 'val' in s]

        if len(loss_list) == 0:
            print('Loss is missing in history')
            return

        ## As loss always exists
        epochs = range(1, len(history.history[loss_list[0]]) + 1)
        # print(epochs)

        ## Loss
        plt.figure(1)
        for l in loss_list:
            plt.plot(epochs, history.history[l], 'b', label='Training loss (' + str(str(format(history.history[l][-1], '.5f'))+')'))
        for l in val_loss_list:
            plt.plot(epochs, history.history[l], 'g', label='Validation loss (' + str(str(format(history.history[l][-1], '.5f'))+')'))

        plt.title('Logarithmic Loss')
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.legend()

        plt.show()
    elif options.prediction:
        dictonary = read.read_dic("kern_files/dictonary.txt")
        H_dictonary = read.read_dic("kern_files/dictonary.txt", True)
        (test_m, test_h) = read.read("kern_files/" + options.prediction + "/thema.krn", dictonary, H_dictonary, 1)
        (test_m, test_h) = read.remove_pauses(test_m, test_h)

        input_size = len(test_h)

        for i in range(0, (features * batch_size) - len(test_m) % (features * batch_size)):
            test_m.append(test_m[input_size - 1])
            test_h.append(test_h[input_size - 1])

        # test_m = np.reshape(test_m, (int(len(test_m) / (features * batch_size)), batch_size, features))
        # test_h = np.reshape(test_h, (int(len(test_h) / (features * batch_size)), batch_size, features))

        model = load_model(model_path)

        output_predictions = []
        for h in range(0, features * batch_size):
            output_predictions.append(test_h[h])

        for m in range(features * batch_size, input_size, features * batch_size):
            p = model.predict_classes(
                np.reshape(np.array(test_m[0:m]), (int(len(test_m[0:m]) / (features * batch_size)), batch_size, features))
                , batch_size)[0]
            for n in range(0, features * batch_size):
                output_predictions.append(np.argmax(p))

        for i in range(0, input_size):
            print(str(i) + " Original melody: " + str(write.FloatToTone(test_m[i], dictonary))
                  + " harmony: " + str(write.FloatToTone(test_h[i], H_dictonary))
                + " prediction: " + write.FloatToTone(output_predictions[i - features * batch_size], H_dictonary))

        # print("--------predictions----")
        # predictions = model.predict_classes(test_m, batch_size=batch_size)[0]
        # print(type(predictions))
        # print(predictions.shape)
        # for i in range(12):
        #     print("{}: {:.2f}%".format(write.FloatToTone(i, H_dictonary), predictions[i]))

        write.write("kern_files/" + options.prediction + "/thema.krn",
                    "output.krn", H_dictonary, output_predictions[0:input_size], 1)


def normalization(nominator, denominator):
    try:
        return nominator / denominator
    except TypeError:
        return 0


def renormalization(nominator, denominator):
    try:
        return nominator * denominator
    except TypeError:
        return 0


if __name__ == '__main__':
    main()
