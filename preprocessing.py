#! /usr/bin/env python3


import matplotlib.pyplot as plt
import read
import re
import subprocess
# import mido


def choice(music_file, dic_file):
    distribution = reading(music_file, dic_file)
    plot_it(distribution, music_file.replace('.krn', '.mid'))
    return input("Pick the voice: ")


def plot_it(data, filename):
    subprocess.call(["timidity", filename])
    
    colors = ['green', 'orange', 'black', 'blue']
    for i in range(0, len(data[0])):
        plt.plot([data[t][i] for t in range(0, 30)], color=colors[i], label=str("Voice " + str(i)))
    plt.legend()
    plt.xlabel('Pitch')
    plt.ylabel('Sample')
    plt.title('Voices distribution')
    plt.grid(True)
    plt.show()
    # mid = mido.MidiFile()
    # port = mido.open_output(filename)
    # for msg in mid.play():
    #     port.send(msg)


def reading(source, dictonary):
    distribution = []
    dic = read.read_dic(dictonary)
    with open(source, 'r') as file:
        for line in file.readlines():
            if str(line).startswith("!") or str(line).startswith("=") or str(line).startswith("*"):
                # comment
                # end of bar
                # tonality and metre
                continue
            else:
                harmony_distribution = (" ".join(line.split())).split(" ")
                del harmony_distribution[0:1]
                h = []
                for s in harmony_distribution:
                    try:
                        h.append(float(toTone(s, dic)))
                    except:
                        h.append(float(0))
                if float(-1) in h and len(distribution) >= 1:
                    h = remove_1(h, distribution[-1])
                distribution.append(h)
    return distribution


def remove_1(vector, previouse):
    try:
        if len(vector) == len(previouse):
            for i in range(0, len(vector)):
                if vector[i] == float(-1) and previouse[i] != float(-1):
                    vector[i] = previouse[i]
    except:
        return vector
    return vector


def toTone(s, dic):
    s = s.replace("\\", '')
    out = float(-1)
    for tone, f in dic.items():
        if re.search(r'' + tone + '', s):
            # print("Find: " + str(s) + ": " + str(tone) + ": " + str(f))
            return f
        else:
            continue
    return out


# -- dont use:
# def dic_repair(dic_filename):
#     dic = read.read_dic("kern_files/dictonary.txt")
#     temp_dic = read.read_dic(dic_filename)
#     print(str(temp_dic))
#     for tone, num in temp_dic.items():
#         for t, f in dic.items():
#             if t == tone:
#                 temp_dic[tone] = dic[t]
#             else:
#                 continue
#     print(str(temp_dic))
#     with open(dic_filename, "w") as file:
#         for tone, num in temp_dic.items():
#             file.write(str(tone) + " : " + str(num) + "\n")


# choice("kern_files/piano_sonata_6/v1.krn", "kern_files/dictonary.txt")

