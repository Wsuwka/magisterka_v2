\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Problematyka i zakres pracy}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Cele pracy}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Analiza przypadk\IeC {\'o}w u\IeC {\.z}ycia r\IeC {\'o}\IeC {\.z}nych technologii do harmonizacji}{5}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Przedstawienie propozycji rozwi\IeC {\k a}zania problemu}{6}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Ocena, czy jednostki LSTM nadaj\IeC {\k a} si\IeC {\k e} do tego typu zada\IeC {\'n} [TYTU\IeC {\L } DO ZMIANY]}{6}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Przedstawienie prototypu programu maj\IeC {\k a}cego dokonywa\IeC {\'c} automatycznej harmonizacji melodii}{6}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}Ocena mo\IeC {\.z}liwo\IeC {\'s}ci wdro\IeC {\.z}enia proponowanych rozwi\IeC {\k a}za\IeC {\'n}, ich warto\IeC {\'s}\IeC {\'c} praktyczna, lokalne i globalne mo\IeC {\.z}liwo\IeC {\'s}ci zastosowania}{6}{subsection.1.2.5}
\contentsline {section}{\numberline {1.3}Metoda badawcza}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Przegl\IeC {\k a}d literatury w dziedzinie}{7}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Automatyczna harmonizacja melodii}{8}{subsection.1.4.1}
\contentsline {subsubsection}{Modele Markov'a}{8}{section*.2}
\contentsline {subsubsection}{Algorytmy genetyczne}{9}{section*.3}
\contentsline {subsubsection}{Rozwi\IeC {\k a}zania rekurencyjne}{9}{section*.4}
\contentsline {subsection}{\numberline {1.4.2}Wady i s\IeC {\l }abe punkty istniej\IeC {\k a}cych rozwi\IeC {\k a}za\IeC {\'n}}{9}{subsection.1.4.2}
\contentsline {subsubsection}{Brak rozwi\IeC {\k a}za\IeC {\'n} uniwersalnych}{10}{section*.5}
\contentsline {subsubsection}{Rygorystyczne funkcje kary dla brzmie\IeC {\'n} atonalnych}{10}{section*.6}
\contentsline {section}{\numberline {1.5}Uk\IeC {\l }ad pracy}{10}{section.1.5}
\contentsline {chapter}{\numberline {2}Idea automatycznej harmonizacji utwor\IeC {\'o}w muzycznych}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}RNN G\IeC {\l }\IeC {\k e}bokie uczenie w procesie harmoniazcji}{12}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Wsteczna propagacja b\IeC {\l }\IeC {\k e}du}{12}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Problem zanikaj\IeC {\k a}cych gradient\IeC {\'o}w}{13}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Rekurencyjne sieci LSTM}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Mo\IeC {\.z}liwo\IeC {\'s}ci jednostek LSTM}{16}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Uczenie maszynowe w procesie automatycznej harmonizacji}{16}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Format danych wej\IeC {\'s}ciowych}{18}{subsection.2.3.1}
\contentsline {subsubsection}{Tworzenie s\IeC {\l }ownika}{18}{section*.7}
\contentsline {subsection}{\numberline {2.3.2}Tworzenie modeli z\nobreakspace {}u\IeC {\.z}yciem biblioteki Keras}{19}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Przewidywanie harmonicznego akompaniamentu}{20}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Ocena algorytmu uczenia}{20}{subsection.2.3.4}
\contentsline {subsubsection}{Dok\IeC {\l }adno\IeC {\'s}\IeC {\'c} klasyfikacji}{20}{section*.8}
\contentsline {subsubsection}{Utrata logarytmiczna}{20}{section*.9}
\contentsline {chapter}{\numberline {3}Opis programu [tytu\IeC {\l } do zmiany!]}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Przygotowanie danych wej\IeC {\'s}ciowych}{23}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Wczytanie danych wej\IeC {\'s}ciowych}{23}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Tworzenie s\IeC {\l }ownika}{23}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Przygotowanie danych wej\IeC {\'s}ciowych}{24}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Model}{25}{section.3.2}
\contentsline {section}{\numberline {3.3}Trening}{25}{section.3.3}
\contentsline {section}{\numberline {3.4}Prognozowanie i klasyfikacja}{25}{section.3.4}
\contentsline {chapter}{\numberline {4}Badania i wyniki [tytu\IeC {\l } do zmiany!]}{26}{chapter.4}
\contentsline {section}{\numberline {4.1}Dob\IeC {\'o}r danych wej\IeC {\'s}ciowych}{26}{section.4.1}
\contentsline {section}{\numberline {4.2}Ocena wyniku dzia\IeC {\l }ania programu}{26}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}miara odleg\IeC {\l }o\IeC {\'s}ci od wyniku oczekiwanego}{27}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}Ocena mo\IeC {\.z}liwo\IeC {\'s}ci wdro\IeC {\.z}enia proponowanego rozwi\IeC {\k a}zania i przysz\IeC {\l }ego rozwoju}{27}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Rozszerzenie mo\IeC {\.z}liwo\IeC {\'s}ci analizy o dok\IeC {\l }adniejszy opis wsp\IeC {\'o}\IeC {\l }brzeminia towrzysz\IeC {\k a}cemu melodii}{27}{subsection.4.3.1}
