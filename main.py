#! /usr/bin/env python3

import read
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Embedding


def main():
    (melody, harmony), dictonary = reading("kern_files/bach1.krn", "kern_files/dictonary.txt")

    #  --------------------- normalization --------------------------  #
    max_value_to_normalize = int(max(dictonary.values())) + 1
    melody = [normalization(k, max_value_to_normalize) for k in melody]
    harmony = [normalization(k, max_value_to_normalize) for k in harmony]
    #  --------------------------------------------------------------  #

    train_m = melody[:int(2*len(melody)/3)]
    test_m = melody[int(2*len(melody)/3):]

    train_h = melody[:int(2*len(harmony)/3)]
    test_h = melody[int(2*len(harmony)/3):]

    max_features = 1024
    print(len(train_m) / 1024)

    model = Sequential()
    model.add(Embedding(max_features, output_dim=len(train_m)))
    model.add(LSTM(128))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.fit(train_m, train_h, batch_size=16, epochs=10)
    score = model.evaluate(test_m, test_h, batch_size=16)

    print(score)


def normalization(nominator, denominator):
    try:
        return nominator / denominator
    except TypeError:
        return 0


def reading(source, dictonary_file):
    dic = {}
    with open(dictonary_file, "r") as file:
        for line in file.readlines():
            line.strip()
            s, f = line.split(":")
            dic[s] = float(f)

    harmony_distribution = []
    header = []
    with open(source, 'r') as file:
        for line in file.readlines():
            if str(line).startswith("!") or str(line).startswith("="):
                # comment
                # end of bar
                continue
            elif str(line).startswith("*"):
                # tonality and metre
                [header.append(item) for item in line.split("\t")]
            else:
                distribution = line.split("\t")
                m = -1
                try:
                    m = float(read.toTone(distribution[len(distribution)], dic))
                except:
                    if len(harmony_distribution) > 1:
                        m = harmony_distribution[len(harmony_distribution) - 1].melody
                    else:
                        m = 0
                h = read.HarmonyDistribution(str(distribution[0]), m)
                del distribution[0:1]
                del distribution[len(distribution) - 1:len(distribution)]
                for s in distribution:
                    try:
                        h.tones.append(float(read.toTone(s, dic)))
                    except:
                        continue
                harmony_distribution.append(h)

    tonality, tonality_level = read.tonality_detector(header)

    return read.data_compiler(harmony_distribution, dic), dic


if __name__ == '__main__':
    main()
